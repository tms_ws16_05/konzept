# DB-Parkplatz App #

---

### Konzept ###
  
Die DB-Parkplatzapp soll bei der Suche nach Parkplätzen in der Nähe von Bahnhöfen helfen.
Das ortsbasierte Suchen soll sowohl durch die Eingabe eines Ortes, als auch durch 
Positionsbestimmung mittels GPS möglich sein. Angezeigt werden Bezeichnung des 
zugehörigen Bahnhofs,  Entfernung sowie  Auslastung und Preise (falls verfügbar). 
Die zugrundeliegenden Informationen werden von der Deutsche Bahn zur Verfügung gestellt.

---

Der Quellcode zu diesem Projekt befindet sich [ hier ](https://bitbucket.org/tms_ws16_05/app/overview).